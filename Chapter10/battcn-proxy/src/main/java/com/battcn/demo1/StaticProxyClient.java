package com.battcn.demo1;


interface Subject {
    void request();
}

class RealSubject implements Subject {

    @Override
    public void request() {
        System.out.println("Google 搜索 battcn ");
    }
}

class ProxySubject implements Subject {

    private Subject realSubject;

    @Override
    public void request() {
        System.out.println("向代理服务器发起请求");
        //用到时候才加载
        if (realSubject == null) {
            realSubject = new RealSubject();
        }
        realSubject.request();
        System.out.println("代理服务器响应请求");
    }
}


/**
 * @author Levin
 */
public class StaticProxyClient {

    public static void main(String[] args) {
        // 使用代理
        Subject subject = new ProxySubject();
        subject.request();
    }
}
