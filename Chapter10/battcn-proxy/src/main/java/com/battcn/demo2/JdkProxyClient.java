package com.battcn.demo2;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


interface Subject {
    /**
     * 查询请求
     *
     * @return
     */
    void request();
}

class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("动态代理 Google 搜索 battcn ");
    }
}

class SubjectHandler implements InvocationHandler {
    private Subject subject;
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("向代理服务器发起请求");
        //如果第一次调用，生成真实主题
        if (subject == null) {
            subject = new RealSubject();
        }
        subject.request();
        //返回真实主题完成实际的操作
        System.out.println("代理服务器响应请求");
        //如果返回值可以直接 return subject.request()
        return null;
    }

    public static Subject createProxy() {
        return (Subject) Proxy.newProxyInstance(
                ClassLoader.getSystemClassLoader(), new Class[]{Subject.class}, new SubjectHandler()
        );
    }
}

/**
 * JDK动态代理
 *
 * @author Levin
 * @create 2017/11/17 0017
 */
public class JdkProxyClient {

    public static void main(String[] args) {
        Subject proxy = SubjectHandler.createProxy();
        proxy.request();
    }
}
