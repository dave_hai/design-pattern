package com.battcn.demo3;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

interface Subject {
    /**
     * 查询请求
     *
     * @return
     */
    void request();
}

class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("CGLIB 动态代理 Google 搜索 battcn ");
    }
}

class SubjectProxy implements MethodInterceptor {

    private Object target;

    /**
     * 创建代理对象
     *
     * @param target 目标对象
     * @return
     */
    public Object getInstance(Object target) {
        this.target = target;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.target.getClass());
        // 回调方法
        enhancer.setCallback(this);
        // 创建代理对象
        return enhancer.create();
    }


    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("向代理服务器发起请求");
        methodProxy.invokeSuper(o, objects);
        System.out.println("代理服务器响应请求");
        return null;
    }
}


/**
 * @author Levin
 * @create 2017/11/17 0017
 */
public class CglibProxyClient {

    public static void main(String[] args) {
        SubjectProxy proxy = new SubjectProxy();
        Subject subject = (RealSubject) proxy.getInstance(new RealSubject());
        subject.request();
    }
}
