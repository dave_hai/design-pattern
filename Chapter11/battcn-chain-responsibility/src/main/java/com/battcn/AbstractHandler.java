package com.battcn;

/**
 * @author Levin
 * @create 2017/11/24 0024
 */
public abstract class AbstractHandler {

    /**
     * 持有后续的责任对象
     */
    private AbstractHandler handler;

    /**
     * 处理请求的方法
     * @param condition
     */
    public abstract void handleRequest(String condition);

    public AbstractHandler getHandler() {
        return handler;
    }

    public void setHandler(AbstractHandler handler) {
        this.handler = handler;
    }
}