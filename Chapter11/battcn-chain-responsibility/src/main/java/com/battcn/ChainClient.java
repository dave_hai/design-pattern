package com.battcn;

/**
 * @author Levin
 * @create 2017/11/21 0021
 */
public class ChainClient {

    public static void main(String[] args) {
        //组装责任链
        AbstractHandler handlerA = new ConcreteHandlerA();
        AbstractHandler handlerB = new ConcreteHandlerB();
        AbstractHandler handlerZ = new ConcreteHandlerZ();
        // 如A处理不掉转交给B
        handlerA.setHandler(handlerB);
        handlerB.setHandler(handlerZ);
        //提交请求
        handlerA.handleRequest("Z");
    }
}
