package com.battcn;

/**
 * 发送命令
 *
 * @author Levin
 * @create 2017/11/27 0027
 */
public interface Command {
    void execute();
}
