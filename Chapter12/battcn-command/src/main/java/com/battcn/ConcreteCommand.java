package com.battcn;

/**
 * @author Levin
 * @create 2017/11/27 0027
 */
public class ConcreteCommand implements Command {

    /**
     * 相应的接收者
     */
    private Receiver receiver = null;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        //通常会转调接收者对象的相应方法，让接收者来真正执行功能
        receiver.action();
    }
}
