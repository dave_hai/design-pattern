package com.battcn;

/**
 * 接收者
 *
 * @author Levin
 * @create 2017/11/27 0027
 */
public class Receiver {

    public void action() {
        System.out.println("执行请求操作");
    }
}
