package com.battcn.demo;


import java.util.ArrayList;
import java.util.List;

interface Command {
    void execute();
}

class PlayCommand implements Command {

    private MusicPlayer musicPlayer;

    public PlayCommand(MusicPlayer musicPlayer) {
        this.musicPlayer = musicPlayer;
    }

    @Override
    public void execute() {
        musicPlayer.play();
    }
}

class SkipCommand implements Command {

    private MusicPlayer musicPlayer;

    public SkipCommand(MusicPlayer musicPlayer) {
        this.musicPlayer = musicPlayer;
    }

    @Override
    public void execute() {
        musicPlayer.skip();
    }
}

class StopCommand implements Command {

    private MusicPlayer musicPlayer;

    public StopCommand(MusicPlayer musicPlayer) {
        this.musicPlayer = musicPlayer;
    }

    @Override
    public void execute() {
        musicPlayer.stop();
    }
}

/**
 * 调用者
 */
class MusicInvoker {

    private Command playCommand;
    private Command skipCommand;
    private Command stopCommand;

    public void setPlayCommand(Command playCommand) {
        this.playCommand = playCommand;
    }

    public void setSkipCommand(Command skipCommand) {
        this.skipCommand = skipCommand;
    }

    public void setStopCommand(Command stopCommand) {
        this.stopCommand = stopCommand;
    }

    public void play() {
        playCommand.execute();
    }

    public void skip() {
        skipCommand.execute();
    }

    public void stop() {
        stopCommand.execute();
    }
}

/**
 * 音乐播放器：接收者
 *
 * @author Levin
 * @date 2017-11-28.
 */
class MusicPlayer {

    public void play() {
        System.out.println("播放...");
    }

    public void skip() {
        System.out.println("跳过...");
    }

    public void stop() {
        System.out.println("停止...");
    }
}


interface MacroCommand extends Command {
    /**
     * 添加一个命令
     */
    void add(Command command);

    /**
     * 删除一个命令
     */
    void remove(Command command);
}

class MacroMusicCommand implements MacroCommand {

    private static final List<Command> COMMANDS = new ArrayList<>();

    @Override
    public void execute() {
        System.out.println("==========回放开始==========");
        COMMANDS.forEach(Command::execute);
        System.out.println("==========回放结束==========");
    }

    @Override
    public void add(Command command) {
        COMMANDS.add(command);
    }

    @Override
    public void remove(Command command) {
        COMMANDS.remove(command);
    }
}

public class MusicPlayerClient {
    public static void main(String[] args) {
        // 创建 Receiver(接收者)
        MusicPlayer musicPlayer = new MusicPlayer();
        // Command(抽象命令类)
        Command playCommand = new PlayCommand(musicPlayer);
        Command skipCommand = new SkipCommand(musicPlayer);
        Command stopCommand = new StopCommand(musicPlayer);
        // 创建 Invoker(调用者)
        //MusicInvoker invoker = new MusicInvoker();
        //invoker.setPlayCommand(playCommand);
        //invoker.setSkipCommand(skipCommand);
        //invoker.setStopCommand(stopCommand);
        // 测试
        //invoker.play();
        //invoker.skip();
        //invoker.stop();
        //invoker.play();
        //invoker.stop();
        MacroCommand macroCommand = new MacroMusicCommand();
        macroCommand.add(playCommand);
        macroCommand.add(skipCommand);
        macroCommand.add(stopCommand);
        macroCommand.execute();

        // 命令类 与 具体命令实现类
        Runnable runnable = () -> System.out.println("关注 battcn 公众号即可免费领取视频");
        // Invoker（调用者） 接收命令
        Thread thread = new Thread(runnable);
        //  调用 start 命令
        thread.start();

        new Thread(()->System.out.println("关注 battcn 公众号即可免费领取视频")).start();
    }
}

