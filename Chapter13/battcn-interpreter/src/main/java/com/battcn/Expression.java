package com.battcn;

/**
 * @author Levin
 * @create 2017/11/29 0029
 */
public interface Expression {
    int interpret();
}