package com.battcn;

/**
 * @author Levin
 * @create 2017/11/29 0029
 */

public class NumberExpression implements Expression {

    private int number;

    public NumberExpression(int i) {
        number = i;
    }

    public NumberExpression(String s) {
        number = Integer.parseInt(s);
    }

    @Override
    public int interpret() {
        return number;
    }

}