package com.battcn;

/**
 * @author Levin
 * @create 2017/11/29 0029
 */

public class PlusExpression implements Expression {
    Expression leftExpression;
    Expression rightExpression;

    public PlusExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public int interpret() {
        return leftExpression.interpret() + rightExpression.interpret();
    }

}