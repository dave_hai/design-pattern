package com.battcn;


import java.util.ArrayList;
import java.util.List;

/**
 * 抽象迭代器
 *
 * @param <E> 需要迭代的对象
 */
interface Iterator<E> {

    /**
     * 判断是否有还有下一个元素
     *
     * @return 存在返回true
     */
    boolean hasNext();

    /**
     * 取出下一个对象
     *
     * @return 对象
     */
    E next();
}

/**
 * 具体迭代器，Iterator的实现类
 */
class MusicIterator<E> implements Iterator<E> {

    private E[] es;
    private int position = 0;

    public MusicIterator(E[] es) {
        this.es = es;
    }

    @Override
    public boolean hasNext() {
        return position != es.length;
    }

    @Override
    public E next() {
        E e = es[position];
        position += 1;
        return e;
    }
}

/**
 * 抽象聚合类（常为  Collection , List , Set 等）
 */
interface AbstractList<E> {

    /**
     * 添加元素
     *
     * @param e 需要添加到集合中的元素
     */
    void add(E e);

    /**
     * 创建迭代器对象
     *
     * @return 创建好的对象
     */
    Iterator<E> createIterator();
}

/**
 * 具体聚合类 （常为 ArrayList , HashSet 等，是抽象聚合类的实现类）
 */
class MusicList implements AbstractList<String> {
    /**
     * 配合演示
     */
    private String[] books = new String[5];
    private int position = 0;


    @Override
    public void add(String name) {
        books[position] = name;
        position += 1;
    }

    @Override
    public Iterator<String> createIterator() {
        return new MusicIterator<>(books);
    }
}


/**
 * 测试工程
 *
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {
        AbstractList<String> list = new MusicList();
        list.add("凉凉");
        list.add("奇谈");
        list.add("红颜");
        list.add("伴虎");
        list.add("在人间");
        Iterator<String> iterator = list.createIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        List<String> list1 = new ArrayList<>();
        java.util.Iterator<String> iterator1 = list1.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}
