package com.battcn;

/**
 * 抽象同事类
 *
 * @author Levin
 * @create 2017/12/4 0004
 */
public abstract class Colleague {

    protected String name;
    protected Mediator mediator;

    Colleague(String name, Mediator mediator) {
        this.name = name;
        this.mediator = mediator;
    }

}
