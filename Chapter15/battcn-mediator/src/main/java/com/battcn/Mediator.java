package com.battcn;

/**
 * 抽象中介者
 *
 * @author Levin
 * @create 2017/12/4 0004
 */
public interface Mediator {

    /**
     *
     * @param content 需要传递的内容
     * @param coll 传递的人
     */
    void contact(String content, Colleague coll);

}
