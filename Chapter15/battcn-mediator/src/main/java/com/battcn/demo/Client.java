package com.battcn.demo;


/**
 * 抽象中介者
 */
interface Mediator {

    /**
     * 让对象之间通讯
     * @param content 传递的内容
     * @param person  传递的对象
     */
    void contact(String content, AbstractPerson person);

}

/**
 * 抽象同事类
 */
abstract class AbstractPerson {

    protected String name;
    protected Mediator mediator;

    AbstractPerson(String name, Mediator mediator) {
        this.name = name;
        this.mediator = mediator;
    }

    public abstract void receiveMessage(String msg);

}

/**
 * 具体同事类：房东
 */
class HouseOwner extends AbstractPerson {

    HouseOwner(String name, Mediator mediator) {
        super(name, mediator);
    }

    /**
     * 与中介者联系
     *
     * @param message 发送的消息
     */
    public void contact(String message) {
        mediator.contact(message, this);
    }

    /**
     * 获取信息
     *
     * @param message 接收到的消息
     */
    @Override
    public void receiveMessage(String message) {
        System.out.println("房主:" + name + ",获得信息：" + message);
    }
}

/**
 * 具体同事类：租客
 */
class Tenant extends AbstractPerson {

    Tenant(String name, Mediator mediator) {
        super(name, mediator);
    }

    /**
     * 与中介者联系
     *
     * @param message 发送的消息
     */
    public void contact(String message) {
        mediator.contact(message, this);
    }

    /**
     * 获取信息
     *
     * @param message 接收到的消息
     */
    @Override
    public void receiveMessage(String message) {
        System.out.println("租客:" + name + ",获得信息：" + message);
    }
}

/**
 * 具体中介者,抽象的实现,用来协调各个同事之间调用
 */
class MediatorStructure implements Mediator {

    private HouseOwner houseOwner;
    private Tenant tenant;

    public HouseOwner getHouseOwner() {
        return houseOwner;
    }

    public void setHouseOwner(HouseOwner houseOwner) {
        this.houseOwner = houseOwner;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Override
    public void contact(String message, AbstractPerson person) {
        //如果是房主，则租房者获得信息
        if (person == houseOwner) {
            tenant.receiveMessage(message);
        } else {
            //反正则是房主获得信息
            houseOwner.receiveMessage(message);
        }
    }
}


/**
 * @author Levin
 * @create 2017/12/4 0004
 */
public class Client {

    public static void main(String[] args) {
        //一个房主、一个租房者、一个中介机构
        MediatorStructure mediator = new MediatorStructure();

        HouseOwner owner = new HouseOwner("小唐", mediator);
        Tenant tenant = new Tenant("小李", mediator);

        mediator.setHouseOwner(owner);
        mediator.setTenant(tenant);

        tenant.contact("房东您好,请问还有三室两厅出粗吗.....");
        owner.contact("还有!你需要租吗?");

    }
}
