package com.battcn.demo1;

/**
 * 备忘录管理者
 *
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Caretaker {
    /**
     * 备忘录对象
     */
    private Memento memento;

    /**
     * 获取备忘录
     *
     * @return 备忘录对象
     */
    public Memento retrieveMemento() {
        return this.memento;
    }

    /**
     * 存储备忘录对象
     */
    public void saveMemento(Memento memento) {
        this.memento = memento;
    }
}