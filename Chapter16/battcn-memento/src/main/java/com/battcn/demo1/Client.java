package com.battcn.demo1;


/**
 * 客户端调用示例
 *
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Client {

    public static void main(String[] args) {
        Originator originator = new Originator();
        Caretaker caretaker = new Caretaker();

        originator.setState("状态A");
        System.out.println("当前状态：" + originator.getState());
        // 存储内部状态
        caretaker.saveMemento(originator.createMemento());
        System.out.println("存档");

        // 改变状态
        originator.setState("状态B");
        System.out.println("当前状态：" + originator.getState());

        // 改变状态
        originator.setState("状态C");
        System.out.println("当前状态：" + originator.getState());
        // 恢复状态
        originator.restoreMemento(caretaker.retrieveMemento());
        System.out.println("读档");

        System.out.println("恢复后状态：" + originator.getState());
    }
}