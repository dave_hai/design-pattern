package com.battcn.demo1;

/**
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }
}