package com.battcn.demo1;

/**
 * 原发器对象
 *
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Originator {
    /**
     * 需要存储的状态，也有不需要存储的状态
     */
    private String state;

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * 创建对象
     *
     * @return 备忘录对象
     */
    public Memento createMemento() {
        return new Memento(state);
    }

    /**
     * 从备忘录中恢复
     *
     * @param memento 恢复的对象
     */
    public void restoreMemento(Memento memento) {
        this.state = memento.getState();
    }
}