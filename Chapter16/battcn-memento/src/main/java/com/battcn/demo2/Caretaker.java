package com.battcn.demo2;

/**
 * 备忘录管理者
 *
 * @author Levin
 */
public class Caretaker {
    /**
     * 备忘录对象
     */
    private MementoIF memento;

    /**
     * 获取备忘录对象
     */
    public MementoIF retrieveMemento() {
        return memento;
    }

    /**
     * 保存备忘录对象
     */
    public void saveMemento(MementoIF memento) {
        this.memento = memento;
    }
}