package com.battcn.demo2;

/**
 * 原发器对象
 *
 * @author Levin
 */
public class Originator {
    /**
     * 需要存储的状态，也有不需要存储的状态
     */
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * 创建一个新的备忘录对象
     */
    public MementoIF createMemento() {
        return new Memento(state);
    }

    /**
     * 发起人恢复到备忘录对象记录的状态
     */
    public void restoreMemento(MementoIF memento) {
        this.setState(((Memento) memento).getState());
    }

    /**
     * 内部类实现备忘录
     * 私有的，只有自己能访问
     */
    private class Memento implements MementoIF {

        private String state;

        /**
         * 构造方法
         */
        private Memento(String state) {
            this.state = state;
        }

        private String getState() {
            return state;
        }

        private void setState(String state) {
            this.state = state;
        }
    }
}