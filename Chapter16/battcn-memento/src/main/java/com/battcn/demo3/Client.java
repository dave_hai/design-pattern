package com.battcn.demo3;

/**
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Client {

    public static void main(String[] args) {
        Originator originator = new Originator();
        // 修改状态
        originator.setState("状态A");
        System.out.println("当前状态：" + originator.getState());

        // 创建备忘录
        MementoIF memento = originator.createMemento();
        // 修改状态
        originator.setState("状态C");
        System.out.println("当前状态：" + originator.getState());

        // 改变状态
        originator.setState("状态B");
        System.out.println("当前状态：" + originator.getState());

        // 按照备忘录恢复对象的状态
        originator.restoreMemento(memento);
        System.out.println("当前状态：" + originator.getState());
    }
}
