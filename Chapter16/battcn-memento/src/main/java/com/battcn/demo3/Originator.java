package com.battcn.demo3;

/**
 * @author Levin
 * @create 2017/12/5 0005
 */
public class Originator {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento createMemento() {
        return new Memento(this);
    }

    public void restoreMemento(MementoIF memento) {
        Memento m = (Memento) memento;
        setState(m.state);
    }

    private class Memento implements MementoIF {
        private String state;

        private Memento(Originator o) {
            this.state = o.state;
        }

        private String getState() {
            return state;
        }

    }
}
