package com.battcn.demo3;


import java.util.Observable;
import java.util.Observer;

class SubscribeReader implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("开始读取:" + ((Publish) o).getMessage());
    }
}

class SubscribeWrite implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("开始写入:" + ((Publish) o).getMessage());
    }
}


class Publish extends Observable {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        //改变通知者的状态
        super.setChanged();
        //调用父类Observable方法，通知所有观察者
        super.notifyObservers();
    }
}


/**
 * @author Levin
 * @date 2017-12-11.
 */
public class Client {

    public static void main(String[] args) {
        Publish publish = new Publish();
        // 遵循FIFO 模型 先进后出
        SubscribeWrite write = new SubscribeWrite();
        SubscribeReader reader = new SubscribeReader();
        publish.addObserver(reader);
        publish.addObserver(write);
        publish.setMessage("Hello Battcn");
        publish.setMessage("QQ：1837307557");
        publish.setMessage("Email：1837307557@qq.com");
    }

}
