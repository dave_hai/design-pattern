package com.battcn.demo4;

import java.util.Arrays;

/**
 * @author Levin
 * @date 2017-12-06.
 */
public class Client {

    public static void main(String[] args) {
        new ThreadLifeCycleListener().concurrentQuery(Arrays.asList("T1", "T2", "T3"));

    }
}
