package com.battcn.demo4;

/**
 * @author Levin
 * @date 2017-12-06.
 */
public interface LifeCycleListener {

    void onEvent(ObservableRunnable.RunnableEvent event);

}
