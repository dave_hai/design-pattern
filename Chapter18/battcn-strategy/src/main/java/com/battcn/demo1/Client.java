package com.battcn.demo1;

interface Strategy {
    /**
     * 策略方法
     */
    public void strategyInterface();
}

class Context {

    private Strategy strategy;

    /**
     * 构造函数，传入一个具体策略对象
     *
     * @param strategy 具体策略对象
     */
    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    /**
     * 策略方法
     */
    public void contextInterface() {
        strategy.strategyInterface();
    }

}

class ConcreteStrategyA implements Strategy {

    @Override
    public void strategyInterface() {
        System.out.println("ConcreteStrategyA");
    }
}

class ConcreteStrategyB implements Strategy {

    @Override
    public void strategyInterface() {
        System.out.println("ConcreteStrategyB");
    }
}
/**
 * @author Levin
 * @create 2017/12/14 0014
 */
public class Client {

    public static void main(String[] args) {
        Strategy strategyA = new ConcreteStrategyA();
        Strategy strategyB = new ConcreteStrategyB();
        Context context = new Context(strategyA);
        context.contextInterface();
        context = new Context(strategyB);
        context.contextInterface();
    }


}
