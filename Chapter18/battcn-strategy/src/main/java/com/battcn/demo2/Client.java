package com.battcn.demo2;

/**
 * 会员优惠策略
 */
interface MemberStrategy {
    /**
     * 计算图书的价格
     *
     * @param booksPrice 图书的原价
     * @return 计算出打折后的价格
     */
    double calcPrice(double booksPrice);
}

class PrimaryMemberStrategy implements MemberStrategy {

    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("对于初级会员的没有折扣");
        return booksPrice;
    }

}

class IntermediateMemberStrategy implements MemberStrategy {

    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("对于中级会员的折扣为10%");
        return booksPrice * 0.9;
    }

}

class AdvancedMemberStrategy implements MemberStrategy {

    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("对于高级会员的折扣为20%");
        return booksPrice * 0.8;
    }
}

class Price {

    /**
     * 持有一个具体的策略对象
     */
    private MemberStrategy strategy;

    /**
     * 构造函数，传入一个具体的策略对象
     *
     * @param strategy 具体的策略对象
     */
    public Price(MemberStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * 计算图书的价格
     *
     * @param booksPrice 图书的原价
     * @return 计算出打折后的价格
     */
    public double quote(double booksPrice) {
        return this.strategy.calcPrice(booksPrice);
    }
}

/**
 * 测试类
 *
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {

        Price price;
        //创建环境
        price = new Price(new PrimaryMemberStrategy());
        //计算价格
        System.out.println("图书的最终价格为：" + price.quote(300));

        //创建环境
        price = new Price(new AdvancedMemberStrategy());
        //计算价格
        System.out.println("图书的最终价格为：" + price.quote(300));

    }

}
