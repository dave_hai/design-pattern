package com.battcn.demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象访问者接口  -- 人
 */
interface Person {

    /**
     * 喂食猫
     *
     * @param cat 被访问的对象
     */
    void feed(Cat cat);

    /**
     * 喂食狗
     *
     * @param dog 被访问的对象
     */
    void feed(Dog dog);
}

/**
 * 具体访问者角色 -- 主人
 */
class Owner implements Person {

    @Override
    public void feed(Cat cat) {
        System.out.println("主人喂食猫");
    }

    @Override
    public void feed(Dog dog) {
        System.out.println("主人喂食狗");
    }
}

/**
 * 具体访问者角色 -- 其他人
 */
class Someone implements Person {

    @Override
    public void feed(Cat cat) {
        System.out.println("其他人喂食猫");
    }

    @Override
    public void feed(Dog dog) {
        System.out.println("其他人喂食狗");
    }
}

/**
 * 抽象节点（元素）角色 -- 宠物
 */
interface Animal {

    /**
     * 具体操作
     *
     * @param person 操作者
     */
    void accept(Person person);
}

/**
 * 具体节点（元素）角色 -- 宠物狗
 */
class Dog implements Animal {

    @Override
    public void accept(Person person) {
        person.feed(this);
        System.out.println("好好吃，汪汪汪！！！");
    }
}


/**
 * 具体节点（元素）角色 -- 宠物猫
 */
class Cat implements Animal {

    @Override
    public void accept(Person person) {
        person.feed(this);
        System.out.println("好好吃，喵喵喵！！！");
    }
}

/**
 * 结构对象角色类 -- 主人家
 */
class Home {
    private List<Animal> nodeList = new ArrayList<>();

    void action(Person person) {
        for (Animal node : nodeList) {
            node.accept(person);
        }
    }

    /**
     * 添加操作
     *
     * @param animal 动物
     */
    void add(Animal animal) {
        nodeList.add(animal);
    }
}


/**
 * @author Levin
 * @create 2017/12/19 0019
 */
public class Client {

    public static void main(String[] args) {
        Home home = new Home();
        home.add(new Dog());
        home.add(new Cat());

        Owner owner = new Owner();
        home.action(owner);

        Someone someone = new Someone();
        home.action(someone);
    }

}
