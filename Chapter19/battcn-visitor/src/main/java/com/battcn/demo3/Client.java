package com.battcn.demo3;

class Dog {

    public void execute() {
        System.out.println("上古 Dog");
    }

}

class DogBaby1 extends Dog {

    @Override
    public void execute() {
        System.out.println("上古Dog第一代子孙");
    }

}

class DogBaby2 extends Dog {

    @Override
    public void execute() {
        System.out.println("上古Dog第二代子孙");
    }

}

/**
 * 动态分派
 *
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {
        Dog baby1 = new DogBaby1();
        baby1.execute();

        Dog baby2 = new DogBaby2();
        baby2.execute();
    }

}