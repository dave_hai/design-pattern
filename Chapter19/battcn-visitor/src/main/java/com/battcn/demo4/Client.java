package com.battcn.demo4;


class Dog {

}

class DogBaby1 extends Dog {

}

class DogBaby2 extends Dog {

}

class Execute {

    public void execute(Dog dog) {
        System.out.println("上古 Dog");
    }

    public void execute(DogBaby1 baby1) {
        System.out.println("上古Dog第一代子孙");
    }

    public void execute(DogBaby2 baby2) {
        System.out.println("上古Dog第二代子孙");
    }

}

/**
 * @author Levin
 * @create 2017/12/19 0019
 */
public class Client {

    public static void main(String[] args) {
        Dog dog = new Dog();
        Dog baby1 = new DogBaby1();
        Dog baby2 = new DogBaby2();

        Execute exe = new Execute();
        exe.execute(dog);
        exe.execute(baby1);
        exe.execute(baby2);
    }

}