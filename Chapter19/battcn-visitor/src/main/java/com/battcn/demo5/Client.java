package com.battcn.demo5;


class Dog {

    public void accept(Execute exe) {
        exe.execute(this);
    }
}

class DogBaby1 extends Dog {

    @Override
    public void accept(Execute exe) {
        exe.execute(this);
    }
}

class DogBaby2 extends Dog {

    @Override
    public void accept(Execute exe) {
        exe.execute(this);
    }
}

class Execute {

    public void execute(Dog dog) {
        System.out.println("上古 Dog");
    }

    public void execute(DogBaby1 baby1) {
        System.out.println("上古Dog第一代子孙");
    }

    public void execute(DogBaby2 baby2) {
        System.out.println("上古Dog第二代子孙");
    }

}

/**
 * 双重分派
 *
 * @author Levin
 * @create 2017/12/19 0019
 */
public class Client {

    public static void main(String[] args) {
        Dog dog = new Dog();
        Dog baby1 = new DogBaby1();
        Dog baby2 = new DogBaby2();

        Execute exe = new Execute();
        dog.accept(exe);
        baby1.accept(exe);
        baby2.accept(exe);
    }
}