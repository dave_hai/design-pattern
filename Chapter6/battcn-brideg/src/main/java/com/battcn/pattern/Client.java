package com.battcn.pattern;


interface Car {
    /**
     * 车辆行驶
     */
    void run();
}

abstract class AbstractRoad {
    protected Car car;

    public void setCar(Car car) {
        this.car = car;
    }

    public abstract void run();
}

class SpeedWay extends AbstractRoad {
    @Override
    public void run() {
        car.run();
        System.out.println("在高速公路上");
    }
}

class Street extends AbstractRoad {
    @Override
    public void run() {
        car.run();
        System.out.println("在市区街道上");
    }
}

class SmallCar implements Car {
    @Override
    public void run() {
        System.out.println("小汽车");
    }
}

class BigTruck implements Car {
    @Override
    public void run() {
        System.out.println("大卡车");
    }
}

abstract class People {

    protected AbstractRoad abstractRoad;

    public void setAbstractRoad(AbstractRoad abstractRoad) {
        this.abstractRoad = abstractRoad;
    }

    public abstract void run();
}

class Man extends People {

    @Override
    public void run() {
        System.out.println("男人开着");
        abstractRoad.run();
    }
}


/**
 * @author Levin
 * @create 2017/11/9 0009
 */
public class Client {

    public static void main(String[] args) {
        Car bigTruck = new BigTruck();
        Car smallCar = new SmallCar();
        AbstractRoad way = new SpeedWay();
        way.setCar(bigTruck);
        way.run();
        way.setCar(smallCar);
        way.run();

        AbstractRoad street = new Street();
        street.setCar(bigTruck);
        street.run();
        street.setCar(smallCar);
        street.run();

        People people = new Man();
        street.setCar(bigTruck);
        people.setAbstractRoad(street);
        people.run();
    }
}
