package com.battcn.test;

class Road {
    public void run() {
        System.out.println("在路上");
    }
}

class SpeedWay extends Road {
    @Override
    public void run() {
        System.out.println("在高速公路上");
    }
}

class Street extends Road {
    @Override
    public void run() {
        System.out.println("在市区街道上");
    }
}

class CarOnSpeedWay extends SpeedWay {
    @Override
    public void run() {
        System.out.println("汽车在高速公路上行驶");
    }
}

class BusOnSpeedWay extends SpeedWay {
    @Override
    public void run() {
        System.out.println("公共汽车在高速公路上行驶");
    }
}


class CarOnStreet extends Street {
    @Override
    public void run() {
        System.out.println("汽车在街道上行驶");
    }
}

class BusOnStreet extends Street {
    @Override
    public void run() {
        System.out.println("公共汽车在街道上行驶");
    }
}

/**
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {
        CarOnSpeedWay carOnSpeedWay = new CarOnSpeedWay();
        carOnSpeedWay.run();

        BusOnSpeedWay busOnSpeedWay = new BusOnSpeedWay();
        busOnSpeedWay.run();

        CarOnStreet carOnStreet = new CarOnStreet();
        carOnStreet.run();

        BusOnStreet busOnStreet = new BusOnStreet();
        busOnStreet.run();
    }
}
