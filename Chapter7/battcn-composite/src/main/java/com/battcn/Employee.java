package com.battcn;

/**
 * 员工接口/也可以是抽象类
 *
 * @author Levin
 */
public interface Employee {

    void add(Employee employee);

    void remove(Employee employee);

    void print();
}
