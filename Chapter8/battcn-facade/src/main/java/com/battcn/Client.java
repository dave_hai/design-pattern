package com.battcn;

/**
 * @author Levin
 * @create 2017/11/14 0014
 */
class ProductService {
    public void pick() {
        System.out.println("挑选小米Mix2尊享版");
    }
}

class PayService {
    public void pay() {
        System.out.println("支付4799元");
    }
}

class FacadeOrder {

    private ProductService productService;
    private PayService payService;

    public FacadeOrder() {
        this.productService = new ProductService();
        this.payService = new PayService();
    }

    public void buyPhone() {
        this.productService.pick();
        System.out.println("添加购物车");
        this.payService.pay();
    }
}

/**
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {
        FacadeOrder order = new FacadeOrder();
        order.buyPhone();
    }

}
