package com.battcn;


import java.util.HashMap;
import java.util.Map;

interface Flyweight {
    void operation(String name);

    String getType();
}

class ConcreteFlyweight implements Flyweight {
    private String type;

    public ConcreteFlyweight(String type) {
        this.type = type;
    }

    @Override
    public void operation(String name) {
        System.out.printf("[类型(内在状态)] - [%s] - [名字(外在状态)] - [%s]\n", type, name);
    }

    @Override
    public String getType() {
        return type;
    }
}


class FlyweightFactory {

    private static final Map<String, Flyweight> FLYWEIGHT_MAP = new HashMap<>();

    public static Flyweight getFlyweight(String type) {
        if (FLYWEIGHT_MAP.containsKey(type)) {
            return FLYWEIGHT_MAP.get(type);
        } else {
            ConcreteFlyweight flyweight = new ConcreteFlyweight(type);
            FLYWEIGHT_MAP.put(type, flyweight);
            return flyweight;
        }
    }

    public static int total() {
        return FLYWEIGHT_MAP.size();
    }
}

/**
 * @author Levin
 */
public class Client {

    public static void main(String[] args) {
        Flyweight fw0 = FlyweightFactory.getFlyweight("战士");
        Flyweight fw1 = FlyweightFactory.getFlyweight("战士");
        Flyweight fw2 = FlyweightFactory.getFlyweight("法师");
        Flyweight fw3 = FlyweightFactory.getFlyweight("坦克");
        fw1.operation("瑞文");
        fw1.operation("鳄鱼");
        fw2.operation("光辉");
        fw3.operation("泰坦");
        fw3.operation("盖伦");
        System.out.printf("[结果(对象对比)] - [%s]\n", fw0 == fw1);
        System.out.printf("[结果(内在状态)] - [%s]\n", fw1.getType());
        System.out.printf("一共召唤 %s 种类型英雄\n", FlyweightFactory.total());

    }
}
